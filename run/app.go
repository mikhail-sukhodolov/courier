package run

import (
	cache "curier/db/dbcache"
	postgres "curier/db/dbpostgres"
	"curier/geo"
	"curier/metrics"
	cservice "curier/module/courier/service"
	cstorage "curier/module/courier/storage"
	"curier/module/courierfacade/controller"
	cfservice "curier/module/courierfacade/service"
	oservice "curier/module/order/service"
	ostorage "curier/module/order/storage"
	"curier/router"
	"curier/server"
	"curier/workers/order"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"os"
)

type App struct {
}

func NewApp() *App {
	return &App{}
}

func (a *App) Run() error {
	// инициализация баз данных
	//redis
	rclient := cache.NewRedisClient(os.Getenv("CACHE_ADDRESS"), os.Getenv("CACHE_PORT"))
	_, err := rclient.Ping().Result()
	if err != nil {
		return err
	}
	//postgres
	postgresDB, err := postgres.NewPostgresDB()
	if err != nil {
		log.Fatal("Error initializing PostgreSQL:", err)
		return err
	}

	////////////////////////////////////////////////////////////////////

	// инициализация метрик
	storageMetrics := metrics.NewStorageMetrics()
	controllerMetrics := metrics.NewControllerMetrics()

	////////////////////////////////////////////////////////////////////

	// инициализация разрешенной зоны
	allowedZone := geo.NewAllowedZone()
	// инициализация запрещенных зон
	disAllowedZones := []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()}

	////////////////////////////////////////////////////////////////////

	// инициализация хранилищ
	var orderStorage ostorage.OrderStorager
	var courierStorage cstorage.CourierStorager

	switch os.Getenv("DB_TYPE") {
	case "postgres":
		orderStorage = ostorage.NewPostgresOrderStorage(postgresDB, storageMetrics)
		courierStorage = cstorage.NewPostgresCourierStorage(postgresDB, storageMetrics)
	case "redis":
		orderStorage = ostorage.NewRedisOrderStorage(rclient)
		courierStorage = cstorage.NewRedisCourierStorage(rclient)
	default:
		log.Fatal("wrong data base type")
	}

	////////////////////////////////////////////////////////////////////

	// инициализация сервиса заказов
	orderService := oservice.NewOrderService(orderStorage, allowedZone, disAllowedZones)

	orderGenerator := order.NewOrderGenerator(orderService)
	orderGenerator.Run()

	oldOrderCleaner := order.NewOrderCleaner(orderService)
	oldOrderCleaner.Run()

	// инициализация сервиса курьеров
	courierSevice := cservice.NewCourierService(courierStorage, allowedZone, disAllowedZones)

	// инициализация фасада сервиса курьеров
	courierFacade := cfservice.NewCourierFacade(courierSevice, orderService)

	// инициализация контроллера курьеров
	courierController := controller.NewCourierController(courierFacade, controllerMetrics)

	////////////////////////////////////////////////////////////////////

	// инициализация роутера
	routes := router.NewRouter(courierController)

	// инициализация сервера
	r := server.NewHTTPServer()

	// инициализация группы роутов
	api := r.Group("/api")

	// инициализация роутов
	routes.CourierAPI(api)
	mainRoute := r.Group("/")
	routes.Swagger(mainRoute)
	routes.Metrics(mainRoute)

	// инициализация статических файлов
	r.NoRoute(gin.WrapH(http.FileServer(http.Dir("public"))))

	// запуск сервера
	serverPort := os.Getenv("SERVER_PORT")

	if os.Getenv("ENV") == "prod" {
		certFile := "/app/certs/cert.pem"
		keyFile := "/app/certs/private.pem"
		return r.RunTLS(":443", certFile, keyFile)
	}
	return r.Run(":" + serverPort)
}
