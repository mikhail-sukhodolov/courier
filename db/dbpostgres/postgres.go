package postgres

import (
	"github.com/jmoiron/sqlx"
	"log"
)

func NewPostgresDB() (*sqlx.DB, error) {
	connStr := "user=postgres password=root dbname=postgres host=db port=5432 sslmode=disable"

	db, err := sqlx.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	query := `
	CREATE EXTENSION IF NOT EXISTS postgis;

	CREATE TABLE IF NOT EXISTS orders (
 
    ID BIGSERIAL PRIMARY KEY,
    PRICE DOUBLE PRECISION,
    DELIVERY_PRICE DOUBLE PRECISION,
    LNG DOUBLE PRECISION,
    LAT DOUBLE PRECISION,
    LOCATION geometry(POINT, 4326),
    IS_DELIVERED BOOLEAN DEFAULT false,
    CREATED_AT TIMESTAMP
);

	CREATE TABLE IF NOT EXISTS courier (
    id INT,
    score INT,
    LOCATION geometry(POINT, 4326),

    CONSTRAINT uc_couriers_id UNIQUE (id));
`

	_, err = db.Exec(query)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("postgres db create")

	return db, nil
}
