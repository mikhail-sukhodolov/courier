package metrics

import "github.com/prometheus/client_golang/prometheus"

type StorageMetrics struct {
	SaveCounter                        prometheus.Counter
	SaveCourierCounter                 prometheus.Counter
	RemoveOldCounter                   prometheus.Counter
	GetByIDCounter                     prometheus.Counter
	GetCountCounter                    prometheus.Counter
	GetByRadiusCounter                 prometheus.Counter
	SetOrderDeliveredCounter           prometheus.Counter
	GetOneCounter                      prometheus.Counter
	GetOneDurationHistogram            prometheus.Histogram
	SaveDurationHistogram              prometheus.Histogram
	SaveCourierDurationHistogram       prometheus.Histogram
	RemoveOldDurationHistogram         prometheus.Histogram
	GetByIDDurationHistogram           prometheus.Histogram
	GetCountDurationHistogram          prometheus.Histogram
	GetByRadiusDurationHistogram       prometheus.Histogram
	SetOrderDeliveredDurationHistogram prometheus.Histogram
}

func NewStorageMetrics() *StorageMetrics {
	counterOpts := prometheus.CounterOpts{
		Namespace: "storage",
	}

	histogramOpts := prometheus.HistogramOpts{
		Namespace: "storage",
		Buckets:   prometheus.DefBuckets,
	}

	return &StorageMetrics{
		SaveCourierCounter:                 prometheus.NewCounter(counterOpts),
		GetOneCounter:                      prometheus.NewCounter(counterOpts),
		SaveCounter:                        prometheus.NewCounter(counterOpts),
		RemoveOldCounter:                   prometheus.NewCounter(counterOpts),
		GetByIDCounter:                     prometheus.NewCounter(counterOpts),
		GetCountCounter:                    prometheus.NewCounter(counterOpts),
		GetByRadiusCounter:                 prometheus.NewCounter(counterOpts),
		SetOrderDeliveredCounter:           prometheus.NewCounter(counterOpts),
		SaveDurationHistogram:              prometheus.NewHistogram(histogramOpts),
		RemoveOldDurationHistogram:         prometheus.NewHistogram(histogramOpts),
		GetByIDDurationHistogram:           prometheus.NewHistogram(histogramOpts),
		GetCountDurationHistogram:          prometheus.NewHistogram(histogramOpts),
		GetByRadiusDurationHistogram:       prometheus.NewHistogram(histogramOpts),
		SetOrderDeliveredDurationHistogram: prometheus.NewHistogram(histogramOpts),
		SaveCourierDurationHistogram:       prometheus.NewHistogram(histogramOpts),
		GetOneDurationHistogram:            prometheus.NewHistogram(histogramOpts),
	}
}

type ControllerMetrics struct {
	GetStatusCounter             prometheus.Counter
	MoveCourierCounter           prometheus.Counter
	GetStatusDurationHistogram   prometheus.Histogram
	MoveCourierDurationHistogram prometheus.Histogram
	TotalOrders                  prometheus.Gauge
}

func NewControllerMetrics() *ControllerMetrics {
	counterOpts := prometheus.CounterOpts{
		Namespace: "controller",
	}

	histogramOpts := prometheus.HistogramOpts{
		Namespace: "controller",
		Buckets:   prometheus.DefBuckets,
	}
	gaugeOpts := prometheus.GaugeOpts{
		Namespace: "controller",
	}

	return &ControllerMetrics{
		GetStatusCounter:             prometheus.NewCounter(counterOpts),
		MoveCourierCounter:           prometheus.NewCounter(counterOpts),
		GetStatusDurationHistogram:   prometheus.NewHistogram(histogramOpts),
		MoveCourierDurationHistogram: prometheus.NewHistogram(histogramOpts),
		TotalOrders:                  prometheus.NewGauge(gaugeOpts),
	}
}
