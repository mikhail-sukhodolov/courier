package models

import (
	cm "curier/module/courier/models"
	om "curier/module/order/models"
)

type CourierStatus struct {
	Courier cm.Courier `json:"courier"`
	Orders  []om.Order `json:"orders"`
}
