package service

import (
	"context"
	cservice "curier/module/courier/service"
	cfacade "curier/module/courierfacade/models"
	oservice "curier/module/order/service"
	"log"
)

const (
	CourierVisibilityRadius     = 2500 // 2.5km
	CourierDeliveredOrderRadius = 5    // 5m
)

type CourierFacer interface {
	MoveCourier(ctx context.Context, direction, zoom int) // отвечает за движение курьера по карте direction - направление движения, zoom - уровень зума
	GetStatus(ctx context.Context) cfacade.CourierStatus  // отвечает за получение статуса курьера и заказов вокруг него
}

// CourierFacade фасад для курьера и заказов вокруг него (для фронта)
type CourierFacade struct {
	courierService cservice.Courierer
	orderService   oservice.Orderer
}

func NewCourierFacade(courierService cservice.Courierer, orderService oservice.Orderer) CourierFacer {
	return &CourierFacade{courierService: courierService, orderService: orderService}
}

func (c *CourierFacade) MoveCourier(ctx context.Context, direction, zoom int) {
	courier, err := c.courierService.GetCourier(ctx)
	if err != nil {
		//log.Println("getting courier error")
		return
	}

	err = c.courierService.MoveCourier(*courier, direction, zoom)
	if err != nil {
		log.Printf("MoveCourier error %v", err)
	}
}

func (c *CourierFacade) GetStatus(ctx context.Context) cfacade.CourierStatus {
	status := cfacade.CourierStatus{}

	// получаем экземпляр курьера
	courier, err := c.courierService.GetCourier(ctx)
	if err != nil {
		log.Printf("getting status error: %s", err)
		return status
	}
	// отправляем его в статус
	status.Courier = *courier

	// проверяем ордера на возможность доставки
	ordersForDelivery, err := c.orderService.GetByRadius(
		ctx,
		courier.Location.Lng,
		courier.Location.Lat,
		CourierDeliveredOrderRadius,
		"m",
	)
	if err != nil {
		log.Printf("set delivery order error, %v", err)
	}

	// если ордера есть, меняем их статус и время жизни
	if len(ordersForDelivery) != 0 {
		for _, order := range ordersForDelivery {
			err = c.orderService.SetOrderDelivered(ctx, &order)
			if err != nil {
				log.Printf("SetOrderDelivered error %v", err)
			}
			// добавляем стоимость ордера к score курьера
			courier.Score += int(order.DeliveryPrice)
			c.courierService.UpgradeCourier(ctx, *courier)
		}
	}

	// получаем ордера в радиусе видимости, отправляем их в статус
	status.Orders, err = c.orderService.GetByRadius(
		ctx,
		status.Courier.Location.Lng,
		status.Courier.Location.Lat,
		CourierVisibilityRadius,
		"m",
	)
	if err != nil {
		log.Printf("get status error,%v", err)
		return cfacade.CourierStatus{}
	}

	return status
}
