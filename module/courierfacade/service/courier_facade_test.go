package service

import (
	"context"
	courierModels "curier/module/courier/models"
	courierMocks "curier/module/courier/service/mocks"
	"curier/module/courierfacade/models"
	orderModels "curier/module/order/models"
	orderMocks "curier/module/order/service/mocks"
	"github.com/stretchr/testify/mock"
	"reflect"
	"testing"
)

func TestCourierFacade_GetStatus(t *testing.T) {

	type fields struct {
		courierService *courierMocks.Courierer
		orderService   *orderMocks.Orderer
	}
	type args struct {
		ctx context.Context
	}

	tests := []struct {
		name   string
		fields fields
		args   args
		want   models.CourierStatus
	}{
		{
			name: "GetStatis_test",
			fields: fields{
				courierService: courierMocks.NewCourierer(t),
				orderService:   orderMocks.NewOrderer(t),
			},
			args: args{ctx: context.Background()},
			want: models.CourierStatus{
				Courier: courierModels.Courier{},
				Orders:  []orderModels.Order{},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			tt.fields.courierService.
				On("GetCourier", tt.args.ctx).
				Maybe().
				Return(&courierModels.Courier{}, nil)
			tt.fields.courierService.
				On("UpgradeCourier", tt.args.ctx, mock.AnythingOfType("courierModels.Courier")).
				Maybe().
				Return(nil)

			tt.fields.orderService.
				On("GetByRadius", tt.args.ctx, 0.0, 0.0, mock.Anything, "m").
				Maybe().
				Return([]orderModels.Order{}, nil)
			tt.fields.orderService.
				On("SetOrderDelivered", tt.args.ctx, &orderModels.Order{}).
				Maybe().
				Return(nil)

			c := &CourierFacade{
				courierService: tt.fields.courierService,
				orderService:   tt.fields.orderService,
			}

			if got := c.GetStatus(tt.args.ctx); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetStatus() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCourierFacade_MoveCourier(t *testing.T) {

	type fields struct {
		courierService *courierMocks.Courierer
		orderService   *orderMocks.Orderer
	}
	type args struct {
		ctx       context.Context
		direction int
		zoom      int
	}

	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "MoveCourier_test",
			fields: fields{
				courierService: courierMocks.NewCourierer(t),
				orderService:   orderMocks.NewOrderer(t),
			},
			args: args{
				ctx:       context.Background(),
				direction: 0,
				zoom:      0,
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			tt.fields.courierService.
				On("GetCourier", tt.args.ctx).
				Maybe().
				Return(&courierModels.Courier{}, nil)

			tt.fields.courierService.
				On("MoveCourier", courierModels.Courier{}, tt.args.direction, tt.args.zoom).
				Maybe().
				Return(nil)

			c := &CourierFacade{
				courierService: tt.fields.courierService,
				orderService:   tt.fields.orderService,
			}
			c.MoveCourier(tt.args.ctx, tt.args.direction, tt.args.zoom)
		})
	}
}
