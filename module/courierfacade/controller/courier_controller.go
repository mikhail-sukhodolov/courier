package controller

import (
	"context"
	"curier/metrics"
	"encoding/json"
	"net/http"
	"time"

	"curier/module/courierfacade/service"
	"github.com/gin-gonic/gin"
)

type CourierController struct {
	courierService service.CourierFacer
	metrics        *metrics.ControllerMetrics
}

func NewCourierController(courierService service.CourierFacer, metrics *metrics.ControllerMetrics) *CourierController {
	return &CourierController{courierService: courierService, metrics: metrics}
}

func (c *CourierController) GetStatus(ctx *gin.Context) {
	//метрика
	startTime := time.Now()
	defer func() {
		duration := time.Since(startTime).Milliseconds()
		c.metrics.GetStatusDurationHistogram.Observe(float64(duration))
	}()
	c.metrics.GetStatusCounter.Inc()

	// установить задержку в 50 миллисекунд
	time.Sleep(time.Millisecond * 50)

	// получить статус курьера из сервиса courierService используя метод GetStatus
	status := c.courierService.GetStatus(ctx)

	// метрика суммарного числа заказов
	c.metrics.TotalOrders.Set(float64(len(status.Orders)))

	// отправить статус курьера в ответ
	ctx.JSON(http.StatusOK, status)
}

func (c *CourierController) MoveCourier(m webSocketMessage) {
	//метрика
	startTime := time.Now()
	defer func() {
		duration := time.Since(startTime).Milliseconds()
		c.metrics.MoveCourierDurationHistogram.Observe(float64(duration))
	}()
	c.metrics.MoveCourierCounter.Inc()

	var cm CourierMove
	var err error
	// получить данные из m.Data и десериализовать их в структуру CourierMove
	data, ok := m.Data.([]byte)
	if !ok {
		//log.Println("error of type determination...")
		return
	}
	err = json.Unmarshal(data, &cm)
	if err != nil {
		//log.Println("Unmarshaling error")
		return
	}

	// вызвать метод MoveCourier у courierService
	c.courierService.MoveCourier(context.Background(), cm.Direction, cm.Zoom)
}
