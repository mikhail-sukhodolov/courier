package service

import (
	"context"
	"curier/geo"
	"curier/module/order/models"
	"curier/module/order/storage/mocks"
	"fmt"
	"github.com/stretchr/testify/mock"
	"reflect"
	"testing"
)

func TestOrderService_GenerateOrder(t *testing.T) {

	type fields struct {
		storage       *mocks.OrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "GenerateOrder_test_ok",
			fields: fields{storage: mocks.NewOrderStorager(t),
				allowedZone:   geo.NewAllowedZone(),
				disabledZones: []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()},
			},
			args:    args{context.Background()},
			wantErr: false,
		},

		{
			name: "GenerateOrder_test_error",
			fields: fields{storage: mocks.NewOrderStorager(t),
				allowedZone:   geo.NewAllowedZone(),
				disabledZones: []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()},
			},
			args:    args{context.Background()},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			tt.fields.storage.
				On("GenerateUniqueID", mock.Anything).
				Return(func(ctx context.Context) (int64, error) {
					if tt.name == "GenerateOrder_test_error" {
						return int64(1), fmt.Errorf("")
					}
					return int64(0), nil
				})

			tt.fields.storage.
				On("Save", tt.args.ctx, mock.AnythingOfType("models.Order"), mock.Anything).
				Maybe().
				Return(nil)

			o := OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}

			if err := o.GenerateOrder(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("GenerateOrder() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestOrderService_GetByRadius(t *testing.T) {

	type fields struct {
		storage       *mocks.OrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx    context.Context
		lng    float64
		lat    float64
		radius float64
		unit   string
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []models.Order
		wantErr bool
	}{
		{
			name: "GetByRadius_test",
			fields: fields{
				storage:       mocks.NewOrderStorager(t),
				allowedZone:   geo.NewAllowedZone(),
				disabledZones: []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone1()},
			},
			args: args{
				ctx:    context.Background(),
				lng:    30.3609,
				lat:    59.9311,
				radius: 2500,
				unit:   "m",
			},
			want:    []models.Order{},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fields.storage.
				On("GetByRadius", tt.args.ctx, tt.args.lng, tt.args.lat, tt.args.radius, tt.args.unit).
				Return(tt.want, nil)

			o := OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}

			got, err := o.GetByRadius(tt.args.ctx, tt.args.lng, tt.args.lat, tt.args.radius, tt.args.unit)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByRadius() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByRadius() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrderService_GetCount(t *testing.T) {

	type fields struct {
		storage       *mocks.OrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		{
			name: "GetCount_test",
			fields: fields{
				storage:       mocks.NewOrderStorager(t),
				allowedZone:   geo.NewAllowedZone(),
				disabledZones: []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()},
			},
			args:    args{context.Background()},
			want:    0,
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			tt.fields.storage.
				On("GetCount", tt.args.ctx).
				Return(tt.want, nil)

			o := OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}

			got, err := o.GetCount(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetCount() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetCount() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrderService_RemoveOldOrders(t *testing.T) {

	type fields struct {
		storage       *mocks.OrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "RemoveOldOrders_test",
			fields: fields{
				storage:       mocks.NewOrderStorager(t),
				allowedZone:   geo.NewAllowedZone(),
				disabledZones: []geo.PolygonChecker{geo.NewDisAllowedZone2(), geo.NewDisAllowedZone1()},
			},
			args: args{
				ctx: context.Background(),
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			tt.fields.storage.
				On("RemoveOldOrders", tt.args.ctx, OrderMaxAge).
				Once().
				Return(nil)

			o := OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}

			if err := o.RemoveOldOrders(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("RemoveOldOrders() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestOrderService_Save(t *testing.T) {

	type fields struct {
		storage       *mocks.OrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx   context.Context
		order models.Order
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Save_test",
			fields: fields{
				storage:       mocks.NewOrderStorager(t),
				allowedZone:   geo.NewAllowedZone(),
				disabledZones: []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()},
			},
			args: args{
				ctx:   context.Background(),
				order: models.Order{},
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			tt.fields.storage.
				On("Save", tt.args.ctx, tt.args.order, OrderMaxAge).
				Return(nil)

			o := OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}
			if err := o.Save(tt.args.ctx, tt.args.order); (err != nil) != tt.wantErr {
				t.Errorf("Save() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestOrderService_SetOrderDelivered(t *testing.T) {

	type fields struct {
		storage       *mocks.OrderStorager
		allowedZone   geo.PolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx   context.Context
		order *models.Order
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "SetOrderDelivered_test",
			fields: fields{
				storage:       mocks.NewOrderStorager(t),
				allowedZone:   geo.NewAllowedZone(),
				disabledZones: []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()},
			},
			args: args{
				ctx:   context.Background(),
				order: &models.Order{},
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			tt.fields.storage.
				On("SetOrderDelivered", tt.args.ctx, tt.args.order).
				Return(nil)

			o := OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}

			if err := o.SetOrderDelivered(tt.args.ctx, tt.args.order); (err != nil) != tt.wantErr {
				t.Errorf("SetOrderDelivered() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
