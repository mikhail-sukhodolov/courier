package storage

import (
	"context"
	"curier/module/order/models"
	"time"
)

//go:generate go run github.com/vektra/mockery/v2@v2.28.2 --name=OrderStorager
type OrderStorager interface {
	Save(ctx context.Context, order models.Order, maxAge time.Duration) error                       // сохранить заказ с временем жизни
	GetByID(ctx context.Context, orderID int64) (*models.Order, error)                              // получить заказ по id
	GenerateUniqueID(ctx context.Context) (int64, error)                                            // сгенерировать уникальный id
	GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) // получить заказы в радиусе от точки
	GetCount(ctx context.Context) (int, error)                                                      // получить количество заказов
	RemoveOldOrders(ctx context.Context, maxAge time.Duration) error
	SetOrderDelivered(ctx context.Context, order *models.Order) error // удалить старые заказы по истечению времени maxAge
}
