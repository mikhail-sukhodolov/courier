package storage

import (
	"context"
	"curier/module/order/models"
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis"
	"log"
	"strconv"
	"time"
)

type RedisOrderStorage struct {
	storage *redis.Client
}

func NewRedisOrderStorage(storage *redis.Client) OrderStorager {
	return &RedisOrderStorage{storage: storage}
}

func (o *RedisOrderStorage) Save(ctx context.Context, order models.Order, maxAge time.Duration) error {
	// save with geo redis
	return o.saveOrderWithGeo(ctx, order, maxAge)
}

func (o *RedisOrderStorage) saveOrderWithGeo(ctx context.Context, order models.Order, maxAge time.Duration) error {
	var err error
	var data []byte

	// сериализуем ордер в json
	data, err = json.Marshal(order)
	// 1. сохраняем ордер в json redis по ключу order:ID с временем жизни maxAge.Это позволяет сохранить
	// данные ордера и обратиться к ним позже по ключу
	key := fmt.Sprintf("order:%d", order.ID)
	o.storage.Set(key, data, maxAge)

	// 2. добавляем ордер в гео индекс используя метод GeoAdd где Name - это ключ ордера, а Longitude и Latitude - координаты
	// Каждый ордер представлен в виде географического местоположения с указанными координатами (широта и долгота).
	// Это позволяет выполнять гео-запросы, такие как поиск ордеров в определенном радиусе от заданных координат
	o.storage.GeoAdd(
		"geo_orders",
		&redis.GeoLocation{
			Name:      fmt.Sprintf("%d", order.ID),
			Latitude:  order.Lat,
			Longitude: order.Lng,
		},
	)

	// 3. добавляем ордер в ZSET (сортированное множество). Каждый ордер представлен в виде элемента множества
	// с указанным score, который представляет время создания ордера. Это позволяет получать количество заказов
	// и выполнять другие операции сортировки и фильтрации заказов
	o.storage.ZAdd("orders", redis.Z{Score: float64(time.Now().UnixNano()), Member: order.ID})

	return err

	/*
		примеры как выглядят списки
		1. Список ордеров в виде ключ - JSON-строка
		Key: order:1
		{ID:1020613 Price:2947.569203977748 DeliveryPrice:413.17233054080214 Lng:30.235509052273706 Lat:59.95962735382639 IsDelivered:false CreatedAt:0001-01-01 00:00:00 +0000 UTC}
		Key: order:2
		{ID:1020625 Price:1360.0272438257036 DeliveryPrice:273.1334379608883 Lng:30.237856350130972 Lat:59.970735478343435 IsDelivered:false CreatedAt:0001-01-01 00:00:00 +0000 UTC}

		-------------------

		2. Гео-индекс ордеров. Name - id заказа

		Key: geo_orders
		{Name:1020328 Longitude:30.38929134607315 Latitude:60.048769802420296 Dist:1997.8629 GeoHash:3719000358974893}
		{Name:1020282 Longitude:30.316523015499115 Latitude:60.05535500799226 Dist:2117.516 GeoHash:3719000548680515}

		-------------------

		3. ZSET ордеров для получения количества заказов и сортировки по времени создания.
		Member - id заказа, Score - время создания ордера в наносекундах

		Key: orders
		Member: 1020613    Score: 1672486032000000000
		Member: 1020625    Score: 1672486033000000000
	*/

}

func (o *RedisOrderStorage) RemoveOldOrders(ctx context.Context, maxAge time.Duration) error {

	// получаем интервал от нуля до текущего времени минус время жизни ордеров
	maxTime := fmt.Sprintf("%d", time.Now().UnixNano()-int64(maxAge))

	// получаем ID всех ордеров в указанном интервале, которые нужно удалить
	listId, err := o.storage.ZRangeByScore("orders", redis.ZRangeBy{Min: "-inf", Max: maxTime}).Result()
	if err != nil {
		return err
	}
	// проверяем срез на пустоту
	if len(listId) == 0 {
		return nil
	}

	// удаляем ордера из геоиндекса по их id
	for _, orderId := range listId {
		_, err := o.storage.ZRem("geo_orders", orderId).Result()
		if err != nil {
			log.Printf("Ошибка удаления ордера из списка geo_orders: %v", err)
		}
	}
	// удалять ордера по ключу не нужно, они будут удалены автоматически по истечению времени жизни

	// удаляем ордера из сортированного множества используя метод ZRemRangeByScore
	// max указывает максимальное значение score по заданному диапазону
	o.storage.ZRemRangeByScore("orders", "-inf", maxTime)
	if err != nil {
		return err
	}
	return nil
}

func (o *RedisOrderStorage) GetByID(ctx context.Context, orderID int64) (*models.Order, error) {
	var err error
	var data []byte
	var order models.Order

	// получаем ордер из redis по ключу order:ID
	key := fmt.Sprintf("order:%d", orderID)
	data, err = o.storage.Get(key).Bytes()
	// проверяем исключение redis.Nil, в этом случае возвращаем nil, nil
	if err == redis.Nil {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	// десериализуем ордер из json
	err = json.Unmarshal(data, &order)
	if err != nil {
		return nil, err
	}

	return &order, nil
}

func (o *RedisOrderStorage) GetCount(ctx context.Context) (int, error) {
	// получаем количество ордеров в упорядоченном множестве используя метод ZCard
	count, err := o.storage.ZCard("orders").Result()
	if err != nil {
		return 0, err
	}
	return int(count), nil
}

func (o *RedisOrderStorage) GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {
	var err error
	var order *models.Order
	var orders []models.Order
	var ordersLocation []redis.GeoLocation

	// используем метод getOrdersByRadius для получения ID заказов в радиусе
	ordersLocation, err = o.getOrdersByRadius(ctx, lng, lat, radius, unit)
	// в случае отсутствия заказов в радиусе метод getOrdersByRadius должен вернуть nil, nil (при ошибке redis.Nil)
	if err == redis.Nil {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	// проходим по списку geo_ордеров, вытягиваем id и получаем по нему структуру ордера из хранилища через метод GetByID
	orders = make([]models.Order, 0, len(ordersLocation))
	for _, orderLocation := range ordersLocation {
		orderId, err := strconv.ParseInt(orderLocation.Name, 10, 64)
		if err != nil {
			return nil, err
		}
		order, err = o.GetByID(ctx, orderId)
		if err != nil {
			return nil, err
		}
		if order != nil {
			orders = append(orders, *order)
		}
	}
	return orders, nil
}

func (o *RedisOrderStorage) getOrdersByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]redis.GeoLocation, error) {
	// в данном методе мы получаем список ордеров в радиусе от точки
	// возвращаем срез ордеров с координатами и расстоянием до точки

	orders, err := o.storage.GeoRadius("geo_orders", lng, lat, &redis.GeoRadiusQuery{
		Radius:      radius,
		Unit:        unit,
		WithCoord:   true,
		WithDist:    true,
		WithGeoHash: true,
	}).Result()
	if err == redis.Nil {
		log.Printf("empty value")
	}
	if err != nil {
		return nil, err
	}
	return orders, nil
}

func (o *RedisOrderStorage) GenerateUniqueID(ctx context.Context) (int64, error) {
	// генерируем уникальный ID для ордера
	// используем для этого redis incr по ключу order:id
	// в качестве ключа "orderkey" можно использовать любую строку, которая уникально идентифицирует этот счетчик заказов в Redis
	id, err := o.storage.Incr("orderkey").Result()
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (o *RedisOrderStorage) SetOrderDelivered(ctx context.Context, order *models.Order) error {
	order.IsDelivered = true
	err := o.Save(ctx, *order, 1*time.Millisecond)
	return err
}
