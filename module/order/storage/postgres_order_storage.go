package storage

import (
	"context"
	"curier/metrics"
	"curier/module/order/models"
	"github.com/jmoiron/sqlx"
	"log"
	"time"
)

type PostgresOrderStorage struct {
	db      *sqlx.DB
	metrics *metrics.StorageMetrics
}

func NewPostgresOrderStorage(db *sqlx.DB, metrics *metrics.StorageMetrics) *PostgresOrderStorage {
	return &PostgresOrderStorage{db: db, metrics: metrics}
}
func (o *PostgresOrderStorage) Save(ctx context.Context, order models.Order, maxAge time.Duration) error {
	//метрика
	startTime := time.Now()
	defer func() {
		duration := time.Since(startTime).Milliseconds()
		o.metrics.SaveDurationHistogram.Observe(float64(duration))
	}()
	o.metrics.SaveCounter.Inc()

	query := `INSERT INTO orders (price, delivery_price, lng, lat, created_at)
VALUES ($1,$2,$3,$4,$5)`
	_, err := o.db.ExecContext(ctx, query, order.Price, order.DeliveryPrice, order.Lng, order.Lat, time.Now())
	if err != nil {
		log.Printf("save err: %s", err)
		return err
	}
	return nil
}

func (o *PostgresOrderStorage) RemoveOldOrders(ctx context.Context, maxAge time.Duration) error {
	//метрика
	startTime := time.Now()
	defer func() {
		duration := time.Since(startTime).Milliseconds()
		o.metrics.RemoveOldDurationHistogram.Observe(float64(duration))
	}()
	o.metrics.RemoveOldCounter.Inc()

	query := `DELETE FROM orders WHERE created_at < $1`
	_, err := o.db.ExecContext(ctx, query, time.Now().Add(-maxAge))
	if err != nil {
		log.Printf("RemoveOld err: %s", err)
		return err
	}
	return nil
}

func (o *PostgresOrderStorage) GetByID(ctx context.Context, orderID int64) (*models.Order, error) {
	//метрика
	startTime := time.Now()
	defer func() {
		duration := time.Since(startTime).Milliseconds()
		o.metrics.GetByIDDurationHistogram.Observe(float64(duration))
	}()
	o.metrics.GetByIDCounter.Inc()

	order := &models.Order{}
	query := `SELECT (*) FROM orders WHERE id = $1 `
	err := o.db.GetContext(ctx, order, query, orderID)
	if err != nil {
		log.Printf("GetById err: %s", err)
		return nil, err
	}
	return order, nil
}

func (o *PostgresOrderStorage) GetCount(ctx context.Context) (int, error) {
	//метрика
	startTime := time.Now()
	defer func() {
		duration := time.Since(startTime).Milliseconds()
		o.metrics.GetCountDurationHistogram.Observe(float64(duration))
	}()
	o.metrics.GetCountCounter.Inc()

	var count int
	query := `SELECT count(*) FROM orders`
	err := o.db.GetContext(ctx, &count, query)
	if err != nil {
		log.Printf("Get count err: %s", err)
		return 0, err
	}
	return count, nil
}

func (o *PostgresOrderStorage) GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {
	//метрика
	startTime := time.Now()
	defer func() {
		duration := time.Since(startTime).Milliseconds()
		o.metrics.GetByRadiusDurationHistogram.Observe(float64(duration))
	}()
	o.metrics.GetByRadiusCounter.Inc()

	orders := []models.Order{}

	query := `
        SELECT id, price, delivery_price, lng, lat, is_delivered, created_at
        FROM orders
		WHERE ST_DWithin(ST_MakePoint(lng, lat)::geography, ST_MakePoint($1, $2)::geography, $3)
    `

	err := o.db.SelectContext(ctx, &orders, query, lng, lat, radius) // Преобразуем радиус в метры
	if err != nil {
		log.Printf("GetByRadius err: %s", err)
		return nil, err
	}

	return orders, nil
}

func (o *PostgresOrderStorage) SetOrderDelivered(ctx context.Context, order *models.Order) error {
	//метрика
	startTime := time.Now()
	defer func() {
		duration := time.Since(startTime).Milliseconds()
		o.metrics.SetOrderDeliveredDurationHistogram.Observe(float64(duration))
	}()
	o.metrics.SetOrderDeliveredCounter.Inc()

	query := `DELETE FROM orders WHERE id = $1`
	_, err := o.db.ExecContext(ctx, query, order.ID)
	if err != nil {
		return err
	}
	return nil
}

// GenerateUniqueID не используется
func (o *PostgresOrderStorage) GenerateUniqueID(ctx context.Context) (int64, error) {
	//TODO implement me
	panic("implement me")
}
