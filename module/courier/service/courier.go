package service

import (
	"context"
	"curier/geo"
	"curier/module/courier/models"
	"curier/module/courier/storage"
	"fmt"
	"math"
)

// Направления движения курьера
const (
	DirectionUp    = 0
	DirectionDown  = 1
	DirectionLeft  = 2
	DirectionRight = 3
)

const (
	DefaultCourierLat = 59.9311
	DefaultCourierLng = 30.3609
)

//go:generate go run github.com/vektra/mockery/v2@v2.28.2 --name=Courierer
type Courierer interface {
	GetCourier(ctx context.Context) (*models.Courier, error)
	MoveCourier(courier models.Courier, direction, zoom int) error
	UpgradeCourier(ctx context.Context, courier models.Courier) error
}

type CourierService struct {
	CourierStorage storage.CourierStorager
	allowedZone    geo.PolygonChecker
	disabledZones  []geo.PolygonChecker
}

func NewCourierService(courierStorage storage.CourierStorager, allowedZone geo.PolygonChecker, disabledZones []geo.PolygonChecker) Courierer {
	return &CourierService{CourierStorage: courierStorage, allowedZone: allowedZone, disabledZones: disabledZones}
}

func (c *CourierService) GetCourier(ctx context.Context) (*models.Courier, error) {
	// получаем курьера из хранилища используя метод GetOne из storage/courier.go
	courier, err := c.CourierStorage.GetOne(ctx)
	if err != nil {
		//создаем дефолтного курьера
		courier = &models.Courier{
			Score:    0,
			Location: models.Point{Lat: DefaultCourierLat, Lng: DefaultCourierLng},
		}
		if err := c.CourierStorage.Save(ctx, *courier); err != nil {
			return nil, err
		}
		return courier, nil
	}

	// проверяем, что курьер находится в разрешенной зоне
	// если нет, то перемещаем его в случайную точку в разрешенной зоне
	if !c.allowedZone.Contains(geo.Point(courier.Location)) {
		courier.Location = models.Point(c.allowedZone.RandomPoint())
	}

	// сохраняем новые координаты курьера
	if err := c.CourierStorage.Save(ctx, *courier); err != nil {
		return nil, err
	}

	return courier, nil
}

// MoveCourier : direction - направление движения курьера, zoom - зум карты
func (c *CourierService) MoveCourier(courier models.Courier, direction, zoom int) error {
	// точность перемещения зависит от зума карты использовать формулу 0.001 / 2^(zoom - 14)
	// 14 - это максимальный зум карты
	delta := 0.001 / math.Pow(2, float64(zoom-14))
	switch direction {
	case DirectionUp:
		courier.Location.Lat += delta
	case DirectionDown:
		courier.Location.Lat -= delta
	case DirectionLeft:
		courier.Location.Lng -= delta
	case DirectionRight:
		courier.Location.Lng += delta
	default:
		return fmt.Errorf("direction: %d has not recognized", direction)
	}

	// далее нужно проверить, что курьер не вышел за границы зоны
	// если вышел, то нужно переместить его в случайную точку внутри зоны
	point := geo.Point{}
	point.Lat, point.Lng = courier.Location.Lat, courier.Location.Lng
	if !geo.CheckPointIsAllowed(point, c.allowedZone, c.disabledZones) {
		point = geo.GetRandomAllowedLocation(c.allowedZone, c.disabledZones)
		courier.Location.Lat, courier.Location.Lng = point.Lat, point.Lng
	}

	// далее сохранить изменения в хранилище
	err := c.CourierStorage.Save(context.Background(), courier)
	if err != nil {
		//log.Printf("courier saving error: %s", err)
		return err
	}

	return nil
}

func (c *CourierService) UpgradeCourier(ctx context.Context, courier models.Courier) error {
	return c.CourierStorage.Save(ctx, courier)
}
