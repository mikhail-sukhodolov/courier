package storage

import (
	"context"
	"curier/metrics"
	"curier/module/courier/models"
	"github.com/jmoiron/sqlx"
	"time"
)

type PostgresCourierStorage struct {
	postgres *sqlx.DB
	metrics  *metrics.StorageMetrics
}

func NewPostgresCourierStorage(postgres *sqlx.DB, metrics *metrics.StorageMetrics) *PostgresCourierStorage {
	return &PostgresCourierStorage{postgres: postgres, metrics: metrics}
}

func (c *PostgresCourierStorage) Save(ctx context.Context, courier models.Courier) error {
	//метрика
	startTime := time.Now()
	defer func() {
		duration := time.Since(startTime).Seconds()
		c.metrics.SaveCourierDurationHistogram.Observe(duration)
	}()

	// SQL запрос для вставки новой записи или обновления существующей, включая географическую точку
	query := `
	-- вставляем запись в таблицу courier с указанием id, score и географической точки location.
	-- географическая точка создается с помощью ST_SetSRID(ST_MakePoint(...), 4326),
	-- где $3 - долгота (lng) и $4 - широта (lat) точки, а 4326 - код пространственной ссылки (SRID).
	INSERT INTO courier (id, score, location)
	VALUES ($1, $2, ST_SetSRID(ST_MakePoint($3, $4), 4326))
	
	-- при конфликте по полю id (если запись с таким id уже существует),
	-- обновляем существующую запись, устанавливая новое значение score и location.
	-- географическая точка обновляется аналогично, как и при вставке.
	ON CONFLICT (id) DO UPDATE SET score = $2, location = ST_SetSRID(ST_MakePoint($3, $4), 4326)
`

	// Выполняем SQL-запрос, подставляя значения параметров.
	// $1 - id курьера, $2 - новое значение score, $3 - долгота, $4 - широта.
	_, err := c.postgres.ExecContext(ctx, query, courier.Id, courier.Score, courier.Location.Lng, courier.Location.Lat)
	if err != nil {
		return err
	}

	return nil
}

func (c *PostgresCourierStorage) GetOne(ctx context.Context) (*models.Courier, error) {
	// метрика
	startTime := time.Now()
	defer func() {
		duration := time.Since(startTime).Seconds()
		c.metrics.GetOneDurationHistogram.Observe(duration)
	}()
	c.metrics.GetOneCounter.Inc()

	// Создаем структуру для хранения результата
	res := &models.Courier{}

	// SQL запрос для выборки одной записи, включая географическую точку
	query := `-- Выбираем следующие поля из таблицы courier:
	-- - id: уникальный идентификатор курьера
	-- - score: оценка курьера
	-- - извлекаем координаты долготы/широты из географической точки, представленной в формате POINT или GEOMETRY
	-- - извлекаем только одну запись с помощью LIMIT 1
	SELECT id, score, 
       ST_X(location) as "location.lng", 
       ST_Y(location) as "location.lat"
	FROM courier
	LIMIT 1
	`
	// Выполняем запрос и получаем результат в структуру res
	err := c.postgres.GetContext(ctx, res, query)
	if err != nil {
		return nil, err
	}
	return res, err
}
