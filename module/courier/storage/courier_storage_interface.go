package storage

import (
	"context"
	"curier/module/courier/models"
)

//go:generate go run github.com/vektra/mockery/v2@v2.28.2 --name=CourierStorager
type CourierStorager interface {
	Save(ctx context.Context, courier models.Courier) error // сохранить курьера по ключу courier
	GetOne(ctx context.Context) (*models.Courier, error)    // получить курьера по ключу courier
}
