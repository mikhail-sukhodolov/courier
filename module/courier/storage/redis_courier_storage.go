package storage

import (
	"context"
	"curier/module/courier/models"
	"encoding/json"
	"github.com/go-redis/redis"
)

type RedisCourierStorage struct {
	storage *redis.Client
}

func NewRedisCourierStorage(storage *redis.Client) *RedisCourierStorage {
	return &RedisCourierStorage{storage: storage}
}

func (c *RedisCourierStorage) Save(ctx context.Context, courier models.Courier) error {
	var err error
	var data []byte

	data, err = json.Marshal(courier)
	if err != nil {
		return err
	}

	return c.storage.Set("courier", data, 0).Err()
}

func (c *RedisCourierStorage) GetOne(ctx context.Context) (*models.Courier, error) {
	var (
		data    []byte
		err     error
		courier models.Courier
	)
	data, err = c.storage.Get("courier").Bytes()
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(data, &courier)
	if err != nil {
		return nil, err
	}

	return &courier, nil
}
