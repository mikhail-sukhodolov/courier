package order

import (
	"context"
	"curier/module/order/service"
	"log"
	"time"
)

const (
	orderCleanInterval = 5 * time.Second
)

// OrderCleaner воркер, который удаляет старые заказы
// используя метод orderService.RemoveOldOrders()
type OrderCleaner struct {
	orderService service.Orderer
}

func NewOrderCleaner(orderService service.Orderer) *OrderCleaner {
	return &OrderCleaner{orderService: orderService}
}

func (o *OrderCleaner) Run() {
	// исользовать горутину и select
	// внутри горутины нужно использовать time.NewTicker()
	// и вызывать метод orderService.RemoveOldOrders()
	// если при удалении заказов произошла ошибка, то нужно вывести ее в лог
	go func() {
		ctx := context.Background()
		ticker := time.NewTicker(orderCleanInterval)

		for range ticker.C {
			// интервал для удаления указываем в order_service
			err := o.orderService.RemoveOldOrders(ctx)
			if err != nil {
				log.Printf("orders cleaning error:%s\n", err)
			}
		}
	}()
}
